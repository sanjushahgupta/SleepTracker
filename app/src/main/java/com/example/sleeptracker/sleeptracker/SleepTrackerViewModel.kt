package com.example.sleeptracker.sleeptracker

import android.app.Application
import android.provider.SyncStateContract.Helpers.insert
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.example.sleeptracker.database.SleepDatabaseDao
import com.example.sleeptracker.database.SleepNight
import com.example.sleeptracker.formatNights
import kotlinx.coroutines.launch

class SleepTrackerViewModel (
    val database: SleepDatabaseDao,
    application: Application
) : AndroidViewModel(application) {
    //varaiable to hold current night
    
    private var tonight = MutableLiveData<SleepNight?>()
    
    //varible TO GET all nights
     val nights = database.getAllNights()

    // Converted nights to Spanned for displaying.
    val nightsString = Transformations.map(nights) { nights ->
        formatNights(nights, application.resources)
    }
    
    init{
        initializeTonight()
    }

    private fun initializeTonight() {
        viewModelScope.launch { 
            tonight.value = getTonightFromDatabase()
        }
    }

    private suspend fun getTonightFromDatabase(): SleepNight? {
        var night = database.getTonight()
        if(night?.endTime != night?.startTime)
        {
            night = null
        }
        return night
        

    }

    fun onStartTracking() {
        viewModelScope.launch {
            // Create a new night, which captures the current time,
            // and insert it into the database.
            val newNight = SleepNight()

            insert(newNight)

            tonight.value = getTonightFromDatabase()
        }
    }
    private suspend fun insert(night: SleepNight) {
        database.insert(night)
    }

    private suspend fun update(night: SleepNight){
        database.update(night)
    }
    fun onStopTracking(){
        viewModelScope.launch {
            val oldNight = tonight.value?: return@launch
            oldNight.endTime = System.currentTimeMillis()
            update(oldNight)
        }
    }



    fun onClear(){
        viewModelScope.launch {
            clear()
            tonight.value = null
        }
    }
    suspend fun clear(){
        database.clear()
    }


    //
    
    
    
    
}
